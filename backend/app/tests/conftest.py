import json
import logging
import os
from datetime import datetime
from typing import Tuple

import pytest
from alembic import command
from alembic.config import Config
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists, drop_database
from starlette.config import environ
from starlette.testclient import TestClient

# This sets `os.environ`, but provides some additional protection.
# If we placed it below the application import, it would raise an error
# informing us that 'TESTING' had already been read from the environment.
from models.users import UserIn, UserOut
from models.users_settings import AvailableLang, AvailableTheme, UserSettingsIn

environ["TESTING"] = "True"

from main import app  # isort:skip
from models import metadata  # isort:skip
from settings import database  # isort:skip


fake = Faker()

logger = logging.getLogger(__name__)


@pytest.fixture(scope="session", autouse=True)
def create_test_database():
    url = str(database.url)
    create_engine(url)
    if database_exists(url):
        drop_database(url)
    create_database(url)  # Create the test database.
    p = os.path.join(os.getcwd(), "alembic.ini")
    m = os.path.join(os.getcwd(), "migrations")
    alembic_config = Config(p)  # Run the migrations.
    alembic_config.set_main_option("script_location", m)
    alembic_config.attributes["configure_logger"] = False
    logger.debug("About to run alembic upgrade in tests")
    command.upgrade(alembic_config, "head")
    yield  # Run the tests.
    drop_database(url)  # Drop the test database.
    logger.debug("Drop db end of session fixture")


@pytest.fixture()
def client() -> TestClient:
    with TestClient(app) as client:
        yield client


@pytest.fixture
def register_one_user(client) -> UserIn:
    # register user
    name = fake.name()
    email = fake.email()
    password = fake.password()
    new_user = UserIn(name=name, email=email, password=password)
    url = app.url_path_for("users_create")
    response = client.post(url, json=new_user.dict())
    assert response.status_code == 201
    expect_out = UserOut(
        **new_user.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None
    ).json()
    assert response.json() == json.loads(expect_out)
    return new_user


@pytest.fixture
def register_two_users(client) -> Tuple[UserIn, UserIn]:
    # register user1
    name1 = fake.name()
    email1 = fake.email()
    password1 = fake.password()
    new_user1 = UserIn(name=name1, email=email1, password=password1)
    url = app.url_path_for("users_create")
    response = client.post(url, json=new_user1.dict())
    assert response.status_code == 201
    expect_out = UserOut(
        **new_user1.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None
    ).json()
    assert response.json() == json.loads(expect_out)
    # register user2
    name2 = fake.name()
    email2 = fake.email()
    password2 = fake.password()
    new_user2 = UserIn(name=name2, email=email2, password=password2)
    url = app.url_path_for("users_create")
    response = client.post(url, json=new_user2.dict())
    assert response.status_code == 201
    expect_out = UserOut(
        **new_user2.dict(),
        time_created=datetime.strptime(
            response.json().get("time_created"), "%Y-%m-%dT%H:%M:%S.%f%z"
        ),
        time_updated=None
    ).json()
    assert response.json() == json.loads(expect_out)

    return new_user1, new_user2


@pytest.fixture
def register_one_user_with_settings(client, register_one_user) -> UserIn:
    url = app.url_path_for(
        "users_settings_create", email_or_name=register_one_user.name
    )
    user_one_settings = UserSettingsIn(
        lang=AvailableLang.en,
        theme=AvailableTheme.dark,
        avatar="https://avatarurl.com/avatar.png",
    )
    response = client.post(url, json=user_one_settings.dict())
    assert response.status_code == 201
    return register_one_user
