import logging
from typing import Optional

from asyncpg import UniqueViolationError
from fastapi import APIRouter, HTTPException
from sqlalchemy import desc, or_
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_409_CONFLICT

from models import users
from models.users import UserIn, UserListPaginated, UserOut, UserPatch
from settings import database, fernet
from utils.password import get_password_hash

router = APIRouter()


logger = logging.getLogger(__name__)


@router.get("/{email_or_name}", response_model=UserOut)
async def users_read(email_or_name: str):
    query = users.select().where(
        or_(users.c.name == email_or_name, users.c.email == email_or_name)
    )
    result = await database.fetch_one(query)
    if not result:
        raise HTTPException(
            status_code=404,
            detail=f"The user {email_or_name} does not exist in the system.",
        )
    else:
        return result


@router.patch("/{_name}", status_code=HTTP_204_NO_CONTENT, response_model=UserOut)
async def users_modify(_name: str, modified_user: UserPatch):
    query = users.select().where(users.c.name == _name)
    result = await database.fetch_one(query)
    if not result:
        raise HTTPException(
            status_code=404, detail=f"The user {_name} does not exist in the system."
        )
    else:
        user_in_db = UserIn(**result)
        update_data = modified_user.dict(skip_defaults=True)
        if "password" in update_data.keys():
            update_data["password"] = get_password_hash(update_data["password"])
        updated_user = user_in_db.copy(update=update_data)
        query = (
            users.update().where(users.c.name == _name).values(**updated_user.dict())
        )
        async with database.transaction():
            try:
                await database.execute(query)
            except UniqueViolationError as e:
                logger.debug(e)
                raise HTTPException(
                    status_code=HTTP_409_CONFLICT, detail="Conflit updating"
                )


@router.post("/", status_code=HTTP_201_CREATED, response_model=UserOut)
async def users_create(user_in: UserIn):
    query = users.select().where(
        or_(users.c.name == user_in.name, users.c.email == user_in.email)
    )
    result = await database.fetch_all(query)
    if len(result):
        raise HTTPException(
            status_code=HTTP_409_CONFLICT, detail="Duplicate email or username"
        )
    else:
        user_in.password = get_password_hash(user_in.password)
        query = (
            users.insert()
            .returning(
                users.c.name, users.c.email, users.c.time_created, users.c.time_updated
            )
            .values(**user_in.dict())
        )
        result = await database.fetch_one(query)
        return result


@router.get("/", response_model=UserListPaginated)
async def users_list(page_size: int = 10, next_page_token: Optional[str] = None):
    if next_page_token is None:
        query = users.select().order_by(desc(users.c.id)).limit(page_size)
    else:
        next_uuid_str: str = fernet.decrypt(next_page_token.encode()).decode()
        query = (
            users.select()
            .where(users.c.id < next_uuid_str)
            .order_by(desc(users.c.id))
            .limit(page_size)
        )
    result = await database.fetch_all(query)
    if len(result) < page_size:
        next_page_token = None
    else:
        a: bytes = str(result[page_size - 1]["id"]).encode()
        next_page_token = fernet.encrypt(a).decode()
    return {"users": result, "next_page_token": next_page_token}
