from fastapi import APIRouter

from celeryapp import celery_app
from models.celery import CeleryMsg

router = APIRouter()


@router.post("/celery1", response_model=CeleryMsg, status_code=201)
def celery1(msg: CeleryMsg):
    """
    Test Celery worker.
    """
    celery_app.send_task("worker.worker.celery1", args=[msg.msg])
    return {"msg": "Word received"}
