import logging

from fastapi import APIRouter
from sqlalchemy import or_, select
from starlette.status import HTTP_201_CREATED

from models import users, users_settings
from models.users_settings import UserSettingsIn
from settings import database

router = APIRouter()


logger = logging.getLogger(__name__)


@router.post("/{email_or_name}", status_code=HTTP_201_CREATED)
async def users_settings_create(email_or_name: str, user_setting: UserSettingsIn):
    # get user uuid
    query = select([users.c.id]).where(
        or_(users.c.name == email_or_name, users.c.email == email_or_name)
    )
    result = await database.fetch_all(query)
    query = (
        users_settings.insert()
        .returning(users_settings.c.settings)
        .values(users_id=result[0].get("id"), settings=user_setting.dict())
    )
    result = await database.fetch_one(query)
    return result.get("settings")


@router.get("/{email_or_name}")
async def users_settings_list(email_or_name: str):
    # get user uuid
    query = select([users.c.id]).where(
        or_(users.c.name == email_or_name, users.c.email == email_or_name)
    )
    result = await database.fetch_one(query)
    query = select([users_settings.c.settings]).where(
        users_settings.c.users_id == result.get("id")
    )
    result = await database.fetch_one(query)
    return result.get("settings")
