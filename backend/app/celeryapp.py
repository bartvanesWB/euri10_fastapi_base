from __future__ import absolute_import

from celery import Celery

import settings

celery_app = Celery(
    "mycelery",
    broker=f"amqp://{settings.RABBITMQ_DEFAULT_USER}:{settings.RABBITMQ_DEFAULT_PASS}@rabbitmq:5672",
    backend="rpc://",
)


celery_app.conf.task_routes = {"worker.worker.celery1": "main-queue"}
celery_app.conf.update(result_expires=3600)
