from celeryapp import celery_app


@celery_app.task(acks_late=True)
def celery1(word: str):
    return f"celery1 task return {word}"
